import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CdkTableModule} from '@angular/cdk/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatGridListModule, MatFormFieldModule} from '@angular/material';
import {MatSelectModule, MatMenuModule} from '@angular/material';
import {MatStepperModule} from '@angular/material/stepper';

import { AppComponent } from './app.component';
import {MatButtonModule, MatCheckboxModule, MatToolbarModule, MatIconModule, MatListModule, MatCardModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import { FunfactsComponent } from './funfacts/funfacts.component';
import { SummaryComponent } from './summary/summary.component';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { InventoryComponent } from './inventory/inventory.component';
import { LeadboardComponent } from './leadboard/leadboard.component';
import { ActionzoneComponent } from './actionzone/actionzone.component';
import { AnalyticsComponent } from './analytics/analytics.component';

@NgModule({
  declarations: [
    AppComponent,
    FunfactsComponent,
    SummaryComponent,
    HomeComponent,
    InventoryComponent,
    LeadboardComponent,
    ActionzoneComponent,
    AnalyticsComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, MatButtonModule, MatCheckboxModule, MatSidenavModule, FormsModule, ReactiveFormsModule,
    CdkTableModule, MatToolbarModule, MatIconModule, MatListModule, MatTabsModule, MatGridListModule, MatCardModule, MatSelectModule,
    MatFormFieldModule, MatMenuModule, HttpClientModule,
    RouterModule.forRoot([
      { path: '', redirectTo : 'app-home', pathMatch: 'full'},
      { path: 'funfacts', component: FunfactsComponent},
      { path: 'summary', component: SummaryComponent},
      { path: 'analytics', component: AnalyticsComponent},
      { path: 'inventory', component: InventoryComponent},
      { path: 'leadboard', component: LeadboardComponent},
      { path: 'actionzone', component: ActionzoneComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
