import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  showFiller = false;
  event: string[] = [];
  opened: boolean;

  constructor(private router: Router, private http: HttpClient,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  ab() {
    this.event.push('open book selected');
  }
  getstatus() {
    this.event.push('Start from Shire');
  }

  getstatus1() {
    this.event.push('Kill some Orcs');
  }

  getstatus2() {
    this.event.push('Throw that goddamn ring in the lava');
  }


}
