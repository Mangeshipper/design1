import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionzoneComponent } from './actionzone.component';

describe('ActionzoneComponent', () => {
  let component: ActionzoneComponent;
  let fixture: ComponentFixture<ActionzoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionzoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionzoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
